#Author: Ecrit par Mi Hyun Lee
Feature: Tests pour E-Commerce application Tutorialsninja

  @register1
  Scenario: Utilisateur peut s inscrire a l application en optant pour l abonnement a la Newsletter
    Given Je suis sur la page Register
    When Je saisis des informations dans tous les champs
      #Datas des informations pour Register
      | First Name | Last Name | E-Mail                 | Telephone    | Password  | PasswordConfirm |
      | Allys      | Justiz    | allys.justiz@gmail.com | 514-222-3333 | allys1234 | allys1234       |
    And Je coche le bouton yes pour s abonne a newsletter
    And Je coche le cas a cocher sur I have read and agree to the Privacy Policy
    Then Un message de reussite s affiche sur la page

  @loginValide
  Scenario Outline: Utilisateur peut se connecter avec des informations d identification valides
    Given Je suis sur la page Login
    When Je sais '<eMailAddress>' et '<password>'
    Then La page My Account s affiche

    Examples: 
      | eMailAddress           | password  |
      | allys.justiz@gmail.com | allys1234 |

  @rechecherProdut
  Scenario Outline: Utilisateur est en mesure de rechercher des produits
    Given Je suis sur la page home
    When Je cherche 'samsung' dans le champ Search
    Then Je valide que les resultat de la liste s affich le produit 'samsung'

  @passerCommande
  Scenario Outline: Utilisateur peut passer une commande
    Given Je suis sur la page Login et se connecte avec '<eMailAddress>' et '<password>'
    When Je recherche '<itemRecherche>' dans le champ Search
    And Clique sur le bouton Add to Cart
    And Clique sur le lien Checkout
    And Valide que le bouton radio est coche sur I want to use an existing address dans le Step2 et clique sur le bouton Continue
    And Valide que le bouton radio est coche sur I want to use an existing address dans le Step3 et clique sur le bouton Continue
    And Valide que le bouton radio est coche sur Flat Rate dans le Step4 et clique sur le bouton Continue
    And Valide que le bouton radio est coche sur Cash On Delivery et coche I have read and agree to the Terms & Conditions dans le Step5 et clique sur le bouton Continue
    And Clique sur le bouton Confirm Order
    Then Je Valide que le message 'Your order has been placed!' s affiche

    Examples: 
      | eMailAddress      | password | itemRecherche |
      | test100@gmail.com | test0000 | HP LP3065     |
