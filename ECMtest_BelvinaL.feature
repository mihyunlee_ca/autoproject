Feature: Tests pour E-Commerce application Tutorialsninja

  @register1
  Scenario: Utilisateur peut s inscrire dans l application en fournissant tous les champs
    Given Je suis sur la page Register
    When Je saisis des informations dans tous les champs
      #Datas des informations pour Register
      | First Name | Last Name | E-Mail            | Telephone   | Password | PasswordConfirm |
      | bbb        | bbb       | bbb.bbb@gmail.com | 555-000-333 | ccc1234  | ccc1234         |
    
    And Je coche le cas a cocher sur I have read and agree to the Privacy Policy
    Then Un message de reussite s affiche sur la page

  @registre2
  Scenario: Utilisateur peut pas enregistre un compte double
    Given Je suis sur la page Register2
    When Je saisis des informations dans tous les champs2
      #Datas des informations pour Register
      | First Name | Last Name | E-Mail            | Telephone   | Password | PasswordConfirm |
      | bbb        | bbb       | bbb.bbb@gmail.com | 555-000-333 | ccc1234  | ccc1234         |
    
    And Je coche le cas a cocher sur I have read and agree to the Privacy Policy2
    Then Un message davertissement s affiche sur la page2 

  @Motdepasse
  Scenario Outline: Utilisateur peut réinitialiser son mot de passe
    Given Je suis sur la page Login 
    When Je clique sur Forgotten Password
    And je saisis ladresse mail associe a mon compte
    And Clique sur le bouton continu
    Then Je Valide que le message 'An email with a confirmation link has been sent your email address.' s affiche


  @produitIndisponible
  Scenario Outline: Utilisateur est en mesure de rechercher des produits
    Given Je suis sur la page home
    When Je cherche 'samsungXX' dans le champ Search
    Then Je valide que le message du produit indisponible 'There is no product that matches the search criteria.'
