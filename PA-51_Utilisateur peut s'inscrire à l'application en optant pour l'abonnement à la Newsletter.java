
public class ECM_ProjectFinal {
	WebDriver driver;

	// Scenario pour Utilisateur peut s'inscrire à l'application en optant pour l'abonnement à la Newsletter
	@Given("Je suis sur la page Register")
	public void je_suis_sur_la_page_register() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://tutorialsninja.com/demo/index.php?route=account/register");
	}

	@When("Je saisis des informations dans tous les champs")
	public void je_saisis_des_informations_dans_tous_les_champs(DataTable dataTable) {
		driver.findElement(By.name("firstname")).sendKeys(dataTable.cell(1, 0));
		driver.findElement(By.name("lastname")).sendKeys(dataTable.cell(1, 1));
		driver.findElement(By.name("email")).sendKeys(dataTable.cell(1, 2));
		driver.findElement(By.name("telephone")).sendKeys(dataTable.cell(1, 3));
		driver.findElement(By.name("password")).sendKeys(dataTable.cell(1, 4));
		driver.findElement(By.name("confirm")).sendKeys(dataTable.cell(1, 5));
	}

	@When("Je coche le bouton yes pour s abonne a newsletter")
	public void je_coche_le_bouton_yes_pour_s_abonne_a_newsletter() {
		driver.findElement(By.xpath("//input[@name='newsletter'][@value='1']")).click();
	}

	@When("Je coche le cas a cocher sur I have read and agree to the Privacy Policy")
	public void je_coche_le_cas_a_cocher_sur_i_have_read_and_agree_to_the_privacy_policy() {
		driver.findElement(By.name("agree")).click();
		driver.findElement(By.xpath("//input[@value='Continue']")).click();
	}

	@Then("Un message de reussite s affiche sur la page")
	public void un_message_de_reussite_s_affiche_sur_la_page() throws InterruptedException {
		Thread.sleep(3000);
		if (driver.getPageSource().contains("Your Account Has Been Created!")) {
			System.out.println("Pass");
		} else {
			System.out.println("Fail");
		}
		driver.findElement(By.xpath("//a[@title='My Account']")).click();
		driver.findElement(By.xpath("//a[text()='Logout']")).click();
		driver.quit();
	}
