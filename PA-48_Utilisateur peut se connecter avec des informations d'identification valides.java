
public class ECM_ProjectFinal {
	WebDriver driver;
	// Scenario pour Utilisateur peut se connecter avec des informations d identification valides
	@Given("Je suis sur la page Login")
	public void je_suis_sur_la_page_login() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://tutorialsninja.com/demo/index.php?route=account/login");
	}
	
	@When("Je sais {string} et {string}")
	public void je_sais_et(String eMailAddress, String password) {
		driver.findElement(By.name("email")).sendKeys(eMailAddress);
		driver.findElement(By.name("password")).sendKeys(password);
		driver.findElement(By.xpath("//input[@value='Login']")).click();
	}
	
	@Then("La page My Account s affiche")
	public void la_page_my_account_s_affiche() {
		boolean textFound = false;
		try {
			driver.findElement(By.xpath("//h2[text()='My Account']"));
			textFound = true;
			System.out.println("Validation OK");
		} catch (Exception e) {
			textFound = false;
			System.out.println("Validation fail");
		}
		driver.findElement(By.xpath("//a[@title='My Account']")).click();
		driver.findElement(By.xpath("//a[text()='Logout']")).click();
		driver.quit();
	}
