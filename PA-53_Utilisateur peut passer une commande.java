
public class ECM_ProjectFinal {
	WebDriver driver;
	// Scenario pour Utilisateur peut passer une commande
	@Given("Je suis sur la page Login et se connecte avec {string} et {string}")
	public void je_suis_sur_la_page_login_et_se_connecte_avec_et(String eMailAddress, String password) {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://tutorialsninja.com/demo/index.php?route=account/login");
		driver.findElement(By.name("email")).sendKeys(eMailAddress);
		driver.findElement(By.name("password")).sendKeys(password);
		driver.findElement(By.xpath("//input[@value='Login']")).click();
	}

	@When("Je recherche {string} dans le champ Search")
	public void je_recherche_dans_le_champ_search(String itemRecherche) throws InterruptedException {
		driver.findElement(By.name("search")).sendKeys(itemRecherche);
		driver.findElement(By.xpath("//span/button[@type='button']")).click();
		Thread.sleep(3000);
	}

	@When("Clique sur le bouton Add to Cart")
	public void clique_sur_le_bouton_add_to_cart() throws InterruptedException {
		driver.findElement(By.xpath("//button/span[text()='Add to Cart']")).click();
		// Selectionner la date livraison
		Thread.sleep(3000);
		driver.findElement(By.xpath("//input[@data-date-format='YYYY-MM-DD']")).sendKeys("2021-12-25");
		driver.findElement(By.id("input-quantity")).clear();
		driver.findElement(By.id("input-quantity")).sendKeys("2");
		driver.findElement(By.xpath("//div/button[text()='Add to Cart']")).click();
	}

	@When("Clique sur le lien Checkout")
	public void clique_sur_le_lien_checkout() {
		driver.findElement(By.xpath("//a[@title='Checkout']")).click();
	}

	@When("Valide que le bouton radio est coche sur I want to use an existing address dans le Step2 et clique sur le bouton Continue")
	public void valide_que_le_bouton_radio_est_coche_sur_i_want_to_use_an_existing_address_dans_le_step2_et_clique_sur_le_bouton_continue()
			throws InterruptedException {
		Thread.sleep(3000);
		if (driver.findElement(By.xpath("//input[@name='payment_address'][@value='existing']")).isSelected()) {
			driver.findElement(By.id("button-payment-address")).click();
		} else {
			driver.findElement(By.xpath("//input[@name='payment_address'][@value='existing']")).click();
			driver.findElement(By.id("button-payment-address")).click();
		}
	}

	@When("Valide que le bouton radio est coche sur I want to use an existing address dans le Step3 et clique sur le bouton Continue")
	public void valide_que_le_bouton_radio_est_coche_sur_i_want_to_use_an_existing_address_dans_le_step3_et_clique_sur_le_bouton_continue()
			throws InterruptedException {
		Thread.sleep(4000);
		if (driver.findElement(By.xpath("//input[@name='shipping_address'][@value='existing']")).isSelected()) {
			driver.findElement(By.id("button-shipping-address")).click();
		} else {
			driver.findElement(By.xpath("//input[@name='shipping_address'][@value='existing']")).click();
			driver.findElement(By.id("button-shipping-address")).click();
		}
	}

	@When("Valide que le bouton radio est coche sur Flat Rate dans le Step4 et clique sur le bouton Continue")
	public void valide_que_le_bouton_radio_est_coche_sur_flat_rate_dans_le_step4_et_clique_sur_le_bouton_continue()
			throws InterruptedException {
		Thread.sleep(3000);
		if (driver.findElement(By.xpath("//input[@name='shipping_method'][@value='flat.flat']")).isSelected()) {
			driver.findElement(By.id("button-shipping-method")).click();
		} else {
			driver.findElement(By.xpath("//input[@name='shipping_method'][@value='flat.flat']")).click();
			driver.findElement(By.id("button-shipping-method")).click();
		}
	}

	@When("Valide que le bouton radio est coche sur Cash On Delivery et coche I have read and agree to the Terms & Conditions dans le Step5 et clique sur le bouton Continue")
	public void valide_que_le_bouton_radio_est_coche_sur_cash_on_delivery_et_coche_i_have_read_and_agree_to_the_terms_conditions_dans_le_step5_et_clique_sur_le_bouton_continue()
			throws InterruptedException {
		Thread.sleep(3000);
		if (driver.findElement(By.xpath("//input[@name='payment_method'][@value='cod']")).isSelected()) {
			driver.findElement(By.name("agree")).click();
		} else {
			driver.findElement(By.xpath("//input[@name='payment_method'][@value='cod']")).click();
			driver.findElement(By.name("agree")).click();
			;
		}
		driver.findElement(By.id("button-payment-method")).click();
	}

	@When("Clique sur le bouton Confirm Order")
	public void clique_sur_le_bouton_confirm_order() throws InterruptedException {
		Thread.sleep(3000);
		driver.findElement(By.id("button-confirm")).click();
	}

	@Then("Je Valide que le message {string} s affiche")
	public void je_valide_que_le_message_s_affiche(String string) throws InterruptedException {
		Thread.sleep(3000);
		if (driver.getPageSource().contains("Your order has been placed!")) {
			System.out.println("Pass");
		} else {
			System.out.println("Fail");
		}
		driver.findElement(By.xpath("//a[@title='My Account']")).click();
		driver.findElement(By.xpath("//a[text()='Logout']")).click();
		driver.quit();
	}
}
	