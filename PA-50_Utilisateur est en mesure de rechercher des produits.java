
public class ECM_ProjectFinal {
	WebDriver driver;
		// Scenario pour Utilisateur est en mesure de rechercher des produits
	@Given("Je suis sur la page home")
	public void je_suis_sur_la_page_home() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://tutorialsninja.com/demo/index.php?route=common/home");
	}

	@When("Je cherche {string} dans le champ Search")
	public void je_cherche_dans_le_champ_search(String item) {
		driver.findElement(By.name("search")).sendKeys(item);
		driver.findElement(By.xpath("//span/button[@type='button']")).click();
	}

	@Then("Je valide que les resultat de la liste s affich le produit {string}")
	public void je_valide_que_les_resultat_de_la_liste_s_affich_le_produit(String item) {
		boolean textFound = false;
		try {
			driver.findElement(By.xpath("//body/div[@id='product-search']/div[@class='row']/div[@id='content']/div[3]"))
					.getText().contains("item");
			textFound = true;
			System.out.println("Validation OK");
		} catch (Exception e) {
			textFound = false;
			System.out.println("Validation fail");
		}
		driver.quit();
	}